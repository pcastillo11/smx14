################## EJEMPLOS DE PRINT ####################

a = input("Dame un numero ->") #Introduce un string
b = input("Dame un numero ->") #Introduce un string
print (a+b) 

c = int(input("Dame un numero ->")) #Introduce un string y lo pasa a entero
d = int(input("Dame un numero ->")) #Introduce un string y lo pasa a entero
print (c+d)

e = input("Dame un numero ->") #Introduce un string
f = int(input("Dame un numero ->") #Introduce un string y lo pasa a entero

print (int(e) + f)
#No se puede implementar "print (e+f)"

