nota = int(input("Dime tu nota -> "))
if nota >= 0 and nota < 4:
    print ("Insuficiente")
elif nota >= 4 and nota < 5:
    print ("Suficiente")
elif nota >= 5 and nota < 6:
    print ("Bien")
elif nota >= 6 and nota < 8:
    print ("Muy bien")
elif nota >= 8 and nota <= 10:
    print ("Excelente")
else:
    print ("Nota no valida")
