
print ("hola mundo")
print ("hola, mundo")
print ("hola", "mundo")

print ("hola" "mundo")

print ("Pablo Castillo")

#print (Pablo Castillo) #Esto es un error porque no es cadena

#separadores
print ("pablo","castillo",sep="",end="\t") #El separador es el espacio pero aqui no es ninguno y el end hace que la linea acabe en un tabulador y no en salto de linea.
print ("pablo","castillo",sep="_",end="@") #El separador ahora es una _ y el end hace que el comando acabe con un @
print ("mi edad es 20") #Esta linea sañdrá a continuación del @ ya que en la anterior linea no le hemos dicho que la siguiente linea empieze en un salto de linea.

#Montar un print (Fundamentos***Programación***en...Python) usando dos funciones print.
print ("Fundamentos","Programación",sep="***",end="***")
print ("en","Python",sep="...")