from traceback import print_tb


print (2 == 2) #Dos enteros son iguales
print (2 == 2.) #Un entero y un float son iguales

var = 0
print (var == 0)
var = 1
print (var == 0)
var = 0
print (var != 0)
var = 1
print (var != 0)

obejasNegras = 10
obejasBlancas = 5
print (obejasNegras > obejasBlancas)
